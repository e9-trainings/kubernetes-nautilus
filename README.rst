====================
Kubernetes at scale!
====================

An easy tutorial that shows you how to scale a Kubernetes infrastructure with a small set of commands.

Getting started
---------------

As a first step, you should launch your `minikube`_ cluster::

    $ minikube start

This repository includes:

* a version of ``AngularJS`` (don't be afraid, JavaScript isn't the target of this tutorial)
* a script file that polls the Kubernetes API service to retrieve the current running pods (and their status)
* some styles (weak protection against our designer)

To start polling the Kubernetes' APIs, you have to expose this folder to the Kubernetes cluster::

    $ kubectl proxy --www=$PWD  # will stay in attached mode

Now you can open a wonderful blank page at: http://localhost:8001/static/

.. _minikube: http://kubernetes.io/docs/getting-started-guides/minikube/

Tutorial
--------

Let's get started!

The first Nautilus
~~~~~~~~~~~~~~~~~~

As a first step, we should launch a new pod with a *nautilus* container::

    $ kubectl run kube-scale --image=evonove/kube-scale:nautilus --port=80 -l name=kube-scale

After the pod is running, you'll see a wonderful Nautilus in the frontend application!
(otherwise, the tutorial is totally broken...).

Fuck yeah, I want a swarm of them!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Probably you don't know what you're doing, but we should agree that you have a lot
of power in our hands. Anyway, we should start scaling up our applications with a simple command::

    $ kubectl scale deployment/kube-scale --replicas=1000

After the command above, the number of the Nautilus swarm must increase (please, tell me that you didn't
launch 1000 instances...).

Time for an update
~~~~~~~~~~~~~~~~~~

Cats. We want an army of cats.

Let's update the deployment descriptor using the command::

    $ kubectl edit deployment/kube-scale

Near the end of the file you should see the ``spec -> containers`` key that includes all data related
to the running container in the pods. Actually it's launching the ``evonove/kube-scale:nautilus``
container, but we want to update the image with a new one that creates an army of cats.

To update your deployment, change the image:

* ``evonove/kube-scale:nautilus`` (->) ``evonove/kube-scale:cat``

Closing your editor is enough to start a new deployment. Congratulations! Now you have an army of
**nothing**! Indeed something bad is happening and we have 1 or 2 failing instances, while the rest of the
infrastructure remains as is. This feature of Kubernetes allow tech ops to stop their deployments
if errors occur.

The Schrödinger cat
~~~~~~~~~~~~~~~~~~~

This is the first time of the Internet that a cat doesn't work. We should delve and analyze the
problem so that we can start spawning an army of cats again. Kubernetes provides a nice command
to describe what is happening within a pod::

    $ kubectl describe pod/<pods_name>  # pods_name is something similar to kube-scale-80920773-phoai

As we can see in the ``Reason`` column, we have a ``Failed`` status with the reason::

    Failed to pull image "evonove/kube-scale:cats": Tag cats not found in repository docker.io/evonove/kube-scale

Ok, Evonove doesn't have a box of ``cats``, so probably we should just find out the proper tag name.
Anyway, so much time is gone and we should rollback our changes otherwise users may be connected to a broken pod::

    $ kubectl rollout undo deployment/kube-scale

And our stupid nautiluses are back.

SAY "cats" again! I dare you! I double-dare you, motherfucker! Say "cats" one more goddamn time!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We don't want cats, no one who is a real programmer wants a fluffy fucking kitten. We want an army of
`Selvano`_. Indeed, the correct container tag is:

* ``evonove/kube-scale:nautilus`` (->) ``evonove/kube-scale:selvano``

.. _Selvano: https://www.facebook.com/silvano.cerza

kill -9 selvano
~~~~~~~~~~~~~~~

Kill 'em all!!

.. code-block:: bash

    $ kubectl delete deployment/kube-scale

After that, the frontend should be empty and you can stop the other process that is running the Kubernetes ``proxy``.

Credits
-------

* Google for Kubernetes
* Silvano Cerza as a cat replacement
